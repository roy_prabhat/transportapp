package com.example.mytransportapp.dataModel;

import android.content.Context;
import android.widget.Toast;

public class FlightDelivery extends DeliveryVehicle {
    @Override
    public void deliver(Context context) {
        Toast.makeText(context, "Delivery By Flight", Toast.LENGTH_SHORT).show();
    }
}
