package com.example.mytransportapp.dataModel;

import android.content.Context;

abstract public class DeliveryVehicle {

    public String DeliveryType;

    public String getDeliveryType() {
        return DeliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        DeliveryType = deliveryType;
    }
    abstract public void deliver(Context context);
}
