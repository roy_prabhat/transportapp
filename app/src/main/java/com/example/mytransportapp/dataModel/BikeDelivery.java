package com.example.mytransportapp.dataModel;

import android.content.Context;
import android.widget.Toast;

public class BikeDelivery extends DeliveryVehicle {
    @Override
    public void deliver(Context context) {
        Toast.makeText(context, "Delivery By Bike", Toast.LENGTH_SHORT).show();
    }
}
