package com.example.mytransportapp.dataModel;

public class DeliveryFactory {

    public static final String BUS = "BUS";
    public static final String BIKE = "BIKE";
    public static final String FLIGHT = "FLIGHT";

    public static DeliveryVehicle getDeliveryTypeClass(String deliveryType) {

        if (deliveryType == null) {
            return null;
        }
        if (deliveryType.equals(BIKE)) {
            return new BikeDelivery();
        } else if (deliveryType.equalsIgnoreCase(BUS)) {
            return new BusDelivery();
        } else if (deliveryType.equalsIgnoreCase(FLIGHT)) {
            return new FlightDelivery();
        }
        return null;
    }
}