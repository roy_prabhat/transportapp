package com.example.mytransportapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.mytransportapp.dataModel.DeliveryFactory;
import com.example.mytransportapp.dataModel.DeliveryVehicle;

public class MainActivity extends AppCompatActivity {

    private Toolbar myToolbar;
    private DeliveryVehicle deliveryVehicle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpToolBar();
    }

    public void setUpToolBar(){
        myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle(R.string.app_name);
    }

    public void deliverByBike(View view) {
        deliveryVehicle = DeliveryFactory.getDeliveryTypeClass(DeliveryFactory.BIKE);
        deliveryVehicle.deliver(getBaseContext());
    }

    public void deliverByFlight(View view) {
        deliveryVehicle = DeliveryFactory.getDeliveryTypeClass(DeliveryFactory.FLIGHT);
        deliveryVehicle.deliver(getBaseContext());
    }

    public void deliverByBus(View view) {
        deliveryVehicle = DeliveryFactory.getDeliveryTypeClass(DeliveryFactory.BUS);
        deliveryVehicle.deliver(getBaseContext());
    }
}
